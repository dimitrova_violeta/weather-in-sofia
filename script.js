$.getJSON('http://api.openweathermap.org/data/2.5/onecall?lat=42.698334&lon=23.319941&exclude=minutely,hourly,alerts&units=metric&appid=bce620b60bf32d52fd8476deb62169da',
function(data){
 console.log(data);

n =  new Date();
y = n.getFullYear();
m = n.getMonth() + 1;
d = n.getDate();

var icon = 'http://api.openweathermap.org/img/w/' + data.current.weather[0].icon + '.png';
var name = data.current.weather[0].main;
var temp = Math.floor(data.current.temp);
var wind = data.current.wind_speed

$(".icon").attr("src", icon);
$(".weather-name").append(name);
$(".temp").append(temp);
$(".wind").append(wind);

var i;
for(i = 1; i <= 3; i++) {
    var icons = 'http://api.openweathermap.org/img/w/' + data.daily[i].weather[0].icon + '.png';
    var names = data.daily[i].weather[0].main;
    var tempsMin = Math.floor(data.daily[i].temp.min);
    var tempsMax = Math.floor(data.daily[i].temp.max);
    var winds = data.daily[i].wind_speed;    

    $(".date" + i).append((d + i) + "." + m + "." + y);
    $(".icon" + i).attr("src", icons);
    $(".weather-name" + i).append(names);
    $(".temp-min" + i).append(tempsMin);
    $(".temp-max" + i).append(tempsMax);
    $(".wind" + i).append(winds);
}
}
);

